let AudioHelper = {};

AudioHelper.addPlayer = (sochcastId) => {
    (function (w, d, s, o, f, js, fjs) {
        w["Decentralized-Player"] = o;
        w[o] = w[o] || function () {
            (w[o].q = w[o].q || []).push(arguments);
        };

        js = d.createElement(s);
        fjs = d.getElementsByTagName(s)[0];

        js.id = o;
        js.src = f;
        js.async = 1;
        fjs.parentNode.insertBefore(js, fjs);
    })(window, document, "script", "w1", "https://notesgensochcast.s3.ap-south-1.amazonaws.com/widget.js");

    setTimeout(() => {
        window.w1("init", { targetElementId: "decentralized_player", id: sochcastId });        
    }, 2000)
}


export default AudioHelper;