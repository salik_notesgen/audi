import React, { Component } from 'react'
import AudioHelper from './Component/AudioHelper';

class App extends Component {
  componentDidMount() {
    AudioHelper.addPlayer('1d29c0ba-9acd-426c-8a32-c7b837bef275');
  }
  render() {
    return (
      <div>
        <div id="decentralized_player"></div>
      </div>
    )
  }
}

export default App
